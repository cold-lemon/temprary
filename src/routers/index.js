import react from 'react'
import ComprehensiveData from '../pages/ComprehensiveData'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

export default [
  {
    path: '/comprehensiveData',
    component: ComprehensiveData,
    children: [],
    name: '综合看板',
    exact: true,
  },
]

