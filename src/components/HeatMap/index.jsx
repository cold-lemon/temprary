import React, { useState, useEffect } from 'react'
import * as echarts from 'echarts'
import ReactECharts from 'echarts-for-react'
import * as moment from 'moment'
import axios from 'axios'

const val = [
  {
    date: '2021-01-01',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-02',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: '总磷'
  },
  {
    date: '2021-01-03',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-04',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-05',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-06',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-07',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-08',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-01-09',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-10',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-11',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-12',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-13',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: 'pH值'
  },
  {
    date: '2021-01-14',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-15',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '总磷'
  },
  {
    date: '2021-01-16',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-17',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-18',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-01-19',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-20',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-21',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-22',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-23',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-24',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-25',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-26',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-27',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-29',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-30',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-01-31',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-01',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-02',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-03',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-04',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-08',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-09',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-11',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-12',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-13',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-14',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-15',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-16',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-17',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-18',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-19',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-20',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-21',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-22',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-23',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-02-24',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-25',
    water_level: null,
    water_status: 1,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-26',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-02-27',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-02-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-03-01',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-03-02',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-03',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-04',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-08',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-09',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-11',
    water_level: null,
    water_status: 4,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-12',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-13',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-14',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-15',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-16',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-17',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-18',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-19',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-20',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-21',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-22',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-23',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-24',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-25',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-26',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-27',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-29',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-30',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-03-31',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-01',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-02',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-03',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-04',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-08',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-09',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-11',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-12',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-13',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-14',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-15',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-16',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-04-17',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-18',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-19',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-04-20',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-04-21',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-04-22',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-23',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-24',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-25',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-26',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-27',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-29',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-04-30',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-01',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-02',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-03',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '高锰酸盐指数'
  },
  {
    date: '2021-05-04',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-08',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-09',
    water_level: null,
    water_status: 2,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-11',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-12',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-13',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-14',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-15',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-16',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-17',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-18',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-19',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '氨氮'
  },
  {
    date: '2021-05-20',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-21',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-22',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-23',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-24',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-25',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-26',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-27',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-29',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-30',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-05-31',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-01',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-02',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-03',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-04',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-08',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-09',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-11',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-12',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-13',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-14',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-15',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-16',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-17',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-18',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-19',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-20',
    water_level: null,
    water_status: 4,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-21',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-22',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-23',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-24',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-25',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-26',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-27',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-28',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-29',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-06-30',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-01',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-02',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-03',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-04',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-05',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-06',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-07',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-08',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-09',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-10',
    water_level: null,
    water_status: 3,
    index: null,
    main_pollution: '溶解氧'
  },
  {
    date: '2021-07-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-07-31',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-01',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-02',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-03',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-04',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-05',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-06',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-07',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-08',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-09',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-10',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-08-31',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-01',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-02',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-03',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-04',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-05',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-06',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-07',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-08',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-09',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-10',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-09-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-01',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-02',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-03',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-04',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-05',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-06',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-07',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-08',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-09',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-10',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-10-31',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-01',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-02',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-03',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-04',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-05',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-06',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-07',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-08',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-09',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-10',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-11-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-01',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-02',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-03',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-04',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-05',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-06',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-07',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-08',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-09',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-10',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-11',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-12',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-13',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-14',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-15',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-16',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-17',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-18',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-19',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-20',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-21',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-22',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-23',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-24',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-25',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-26',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-27',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-28',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-29',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-30',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  },
  {
    date: '2021-12-31',
    water_level: null,
    water_status: 0,
    index: null,
    main_pollution: ''
  }
]

function HeatMap() {
  const [chartOption, setChartOption] = useState({})
  const [chartVal, serChartVal] = useState([])

  useEffect(() => {
    let data = []
    val.map((item) => {
      let arr = [
        item.date,
        item.water_status ? item.water_status : 0,
        item.main_pollution
      ]
      data.push(arr)
    })
    setChartOption(dOption)
    serChartVal(data)
    return () => {}
  }, [])

  const dOption = {
    tooltip: {},
    visualMap: {
      min: 0,
      max: 6,
      type: 'piecewise',
      categories: [1, 2, 3, 4, 5],
      dimension: 1,
      show: false,
      color: ['#F82504', '#FB9B03', '#FFFC00', '#46F900', '#36CDFF']
    },
    calendar: {
      //日历
      top: 120,
      left: 30,
      right: 30,
      cellSize: ['50', 'auto'], //格子宽高
      range: moment().format('YYYY'),
      itemStyle: {
        borderWidth: 0.5,
        borderColor: '#E7E9EB'
      },
      splitLine: {
        lineStyle: {
          color: '#D8E0E7'
        }
      },
      dayLabel: {
        nameMap: 'cn'
      },
      monthLabel: {
        nameMap: 'cn'
      },
      yearLabel: { show: false }
    },
    series: {
      type: 'heatmap',
      coordinateSystem: 'calendar',
      data: chartVal
    }
  }

  const drawChart = () => {
    // setChartOption(options)
  }

  return (
    <div>
      <ReactECharts option={chartOption}></ReactECharts>
    </div>
  )
}

export default HeatMap
