import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import ComprehensiveData from './pages/ComprehensiveData' 

function App() {
  return (
    <Router>
    <Switch>
      <Route exact path='/' component={ ComprehensiveData } />
    </Switch>
  </Router>

  );
}

export default App;
