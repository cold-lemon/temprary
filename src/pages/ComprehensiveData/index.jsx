import React, { useState, useEffect } from 'react'
import { Layout, Tabs, Tree, Input, Menu, Breadcrumb } from 'antd'
import MenuTree from '../../components/MenuTree/index.jsx'
import HeatMap from '../../components/HeatMap/index.jsx'

import './index.css'

const { TabPane } = Tabs
const { Header, Footer, Sider, Content } = Layout
const { Search } = Input

function ComprehensiveData() {
  useEffect(() => {}, [])

  function tabsChange(key) {
    console.log(key)
  }

  return (
    <div className="">
      <Layout>
        <Sider width={210} theme="light" collapsible={true}>
          <Tabs onChange={tabsChange} type="card" size="small">
            <TabPane tab="Tab 1" key="1">
              <div className="table-one">
                <MenuTree></MenuTree>
              </div>
            </TabPane>
            <TabPane tab="Tab 2" key="2">
              Content of Tab Pane 2
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              Content of Tab Pane 3
            </TabPane>
          </Tabs>
        </Sider>

        <Layout>
          <Header theme="light" style={{ background: '#fff', padding: '0px' }}>
            <Content style={{ padding: '0 5px' }}>
              <Breadcrumb style={{ margin: '16px 0' }}>
                <Breadcrumb.Item>Home</Breadcrumb.Item>
                <Breadcrumb.Item>List</Breadcrumb.Item>
                <Breadcrumb.Item>App</Breadcrumb.Item>
              </Breadcrumb>
            </Content>
          </Header>
          <Content className="site-layout-content">
            <HeatMap></HeatMap>
          </Content>
          <Footer>Footer</Footer>
        </Layout>
      </Layout>
    </div>
  )
}

export default ComprehensiveData
